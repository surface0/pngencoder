﻿using System.Security.Cryptography;

namespace PNGEncoder
{
    public class CRC32 : HashAlgorithm
    {
        private const uint CRC32_MASK = 0xFFFFFFFF;

        private static uint[] CRC32Table;

        private uint crc;

        public CRC32()
        {
            base.HashSizeValue = 32;
            Initialize();
        }

        private void makeTable()
        {
            CRC32Table = new uint[256];
            for (uint i = 0; i < 256; i++)
            {
                uint c = i;
                for (var j = 0; j < 8; j++)
                {
                    c = (c & 1) > 0 ? (0xEDB88320 ^ (c >> 1)) : (c >> 1);
                }
                CRC32Table[i] = c;
            }
        }

        protected override void HashCore(byte[] array, int ibStart, int cbSize)
        {
            unchecked
            {
                while (--cbSize >= 0)
                {
                    var idx = (crc ^ (uint)array[ibStart++]) & 0xFF;
                    crc = (CRC32Table[idx] ^ (crc >> 8));
                }
            }
        }

        protected override byte[] HashFinal()
        {
            crc ^= CRC32_MASK;
            HashValue = new byte[] {
                (byte)((crc >> 24) & 0xFF), 
                (byte)((crc >> 16) & 0xFF), 
                (byte)((crc >>  8) & 0xFF), 
                (byte)( crc        & 0xFF)
            };
            return HashValue;
        }

        public override void Initialize()
        {
            crc = CRC32_MASK;

            if (CRC32Table == null)
            {
                makeTable();
            }
        }
    }
}